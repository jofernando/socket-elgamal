import socket
import os
import sys
import ElGamal as EG
import time

s = socket.socket()
host="172.31.0.17"
port = 13132
s.bind((host,port))
s.listen(10)
c = None
while True:
    if(c == None):
        c, addr = s.accept()
        print("Cliente conectado",addr)
    conteudo = c.recv(8192).decode()
    if(conteudo == "fim"):
        break
    ini = time.time()
    key = EG.encryptionKey(int(conteudo))
    fim = time.time()
    # c.send(("Chave: "+str(key)+" tempo: "+str(fim-ini)).encode('UTF-8'))
    # print("Chave: "+str(key)+" tempo: "+str(fim-ini))
    c.send(("Chave: "+str(key.p)+" tempo: "+str(fim-ini)).encode('UTF-8'))
    print("Chave: "+str(key.p)+" tempo: "+str(fim-ini))



c.close()    
s.close()
