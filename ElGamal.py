from Crypto import Random
from Crypto.Random import random
from Crypto.PublicKey import ElGamal
from Crypto.Util.number import GCD
from Crypto.Hash import SHA
import time
from random import randrange

def encryptionKey(bits):
    return ElGamal.generate(bits, Random.new().read)
